#ifndef FW_H
#define FW_H

#include "fwdef.h"
#include "servo.h"

#include <handler/evt.h>
#include <ctl/ewma.h>
#include <hw.h>

#define CHANNELS 2

#define CTL_PERIOD_VAL gen_make(VAL_T, CTL_PERIOD)

typedef ewma_param_t(VAL_T) Tflt_param_t;
extern Tflt_param_t Tenv_flt_param;

typedef struct {
  VAL_T min;
  VAL_T max;
} Tenv_t;

extern Tenv_t Tenv[CHANNELS];

void ctl_step(void);

#define temp_valid(type, value) (gen_gt(type, value, gen_make(type, T_MIN)) && gen_lt(type, value, gen_make(type, T_MAX)))

typedef servo_param_t(VAL_T) flap_param_t;
extern flap_param_t flap_param;
typedef servo_state_t(VAL_T) flap_state_t;
extern flap_state_t flap_state[CHANNELS];

#define driver_close gen_0(VAL_T)
#define driver_open gen_1(VAL_T)

void driver_init(void);
void driver_step(void);

enum {
  any_ev = 0xff,
  //sys_ev_rtc = 1 << 0,
  srt_ev_req = 1 << 1,
};

#define main_resume()                  \
  mcu_modes_on_off(mcu_modes_notouch,  \
                   mcu_sleep_on_exit)

#define main_suspend()                 \
  mcu_modes_on_off(mcu_sleep_on_exit,  \
                   mcu_modes_notouch); \
  mcu_wait_for_interrupt()

extern volatile uint8_t events;

void srt_on_req(void);

#endif /* FW_H */
