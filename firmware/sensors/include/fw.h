#ifndef FW_H
#define FW_H

#include "fwdef.h"

#include <handler/evt.h>
#include <ctl/ewma.h>
#include <hw.h>

#define CTL_PERIOD_VAL gen_make(VAL_T, CTL_PERIOD)

typedef ewma_param_t(VAL_T) Tflt_param_t;
extern Tflt_param_t Tair_flt_param;

void ctl_step(void);

#define temp_valid(T) gen_gt(VAL_T, T, gen_make(VAL_T, T_INVALID))

void driver_init(void);
void driver_step(void);

enum {
  any_ev = 0xff,
  //sys_ev_rtc = 1 << 0,
  srt_ev_req = 1 << 1,
};

#define main_resume()                  \
  mcu_modes_on_off(mcu_modes_notouch,  \
                   mcu_sleep_on_exit)

#define main_suspend()                 \
  mcu_modes_on_off(mcu_sleep_on_exit,  \
                   mcu_modes_notouch); \
  mcu_wait_for_interrupt()

extern volatile uint8_t events;

void srt_on_req(void);

#endif /* FW_H */
