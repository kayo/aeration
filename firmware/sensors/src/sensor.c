#include <ctl/type.h>
#include <ctl/util.h>
#include <ctl/med.h>

#include "hw.h"
#include "fw.h"
#include "upar/params.h"

#define toU(type, a)                     \
  scale_val_const(type, a,               \
                  ADC_MIN, ADC_MAX,      \
                  ADC_U_MIN, ADC_U_MAX)

#define toA(type, v)                     \
  scale_val_const(type, v,               \
                  ADC_U_MIN, ADC_U_MAX,  \
                  ADC_MIN, ADC_MAX)

#if 1
#define Avg_Slope 4.3
#define V30 1.43

/* T = (30 + V30 / Avg_Slope) - adc * (ADC_U_MAX / (ADC_MAX * Avg_Slope)) */
#define toTmcu(type, adc)                           \
  gen_sub(type,                                     \
          gen_make(type, V30 / Avg_Slope + 30),     \
          gen_mul(type, adc,                        \
                  gen_make(type, ADC_U_MAX /        \
                           (ADC_MAX * Avg_Slope))))
#else
#define toTmcu(type, adc)                               \
  scale_val(type, adc,                                  \
            gen_make(type, adc_cal_temp_30C_at_3V3()),  \
            gen_make(type, adc_cal_temp_110C_at_3V3()), \
            gen_make(type, 30), gen_make(type, 110))
#endif

#define A2D(A) (((double)(A) - (double)(ADC_MIN)) * (1.0 / ((double)(ADC_MAX) - (double)(ADC_MIN))))
#define D2A(D) ((D) * ((double)(ADC_MAX) - (double)(ADC_MIN)) + (double)(ADC_MIN))

#include "thermistor.h"

#define T_A2R(A) A2R_HI(A, T_RS, T_RP)
#define T_R2A(R) R2A_HI(R, T_RS, T_RP)

#define T_T2A(C) T_R2A(BETA_K2R(C2K(C), T_BETA, T_RN, C2K(T_NOM)))
#define T_A2T(A) K2C(BETA_R2K(T_A2R(A), T_BETA, T_RN, C2K(T_NOM)))

#include "ctl.h"

#define toT(type, A) lookup_val(type, T, A)

Tflt_param_t Tair_flt_param, Toil_flt_param, Ths_flt_param;
#define Tflt_state(TX) TX##_flt_state
static ewma_state_t(VAL_T) Tflt_state(int__Text1), Tflt_state(int__Text2), Tflt_state(int__Text3), Tflt_state(int__Text4), Tflt_state(int__Ths);

#define Tflt_init(TX) ewma_init(VAL_T, &TX##_flt_state, TX = raw_##TX)
#define Tflt_step(TX, PX) TX = ewma_step(VAL_T, &PX##_flt_param, &TX##_flt_state, raw_##TX)

bool inited = false;

static void adc_conv(VAL_T data[]) {
  int__Uref = toU(VAL_T, data[ADC_INDEX_U_REF]);
  int__Tmcu = toTmcu(VAL_T, data[ADC_INDEX_T_MCU]);

  VAL_T raw_int__Text1 = toT(VAL_T, data[ADC_INDEX_T_1]);
  VAL_T raw_int__Text2 = toT(VAL_T, data[ADC_INDEX_T_2]);
  VAL_T raw_int__Text3 = toT(VAL_T, data[ADC_INDEX_T_3]);
  VAL_T raw_int__Text4 = toT(VAL_T, data[ADC_INDEX_T_4]);

  if (inited) {
    Tflt_step(int__Text1, Tair);
    Tflt_step(int__Text2, Tair);
    Tflt_step(int__Text3, Tair);
    Tflt_step(int__Text4, Tair);
  } else {
    Tflt_init(int__Text1);
    Tflt_init(int__Text2);
    Tflt_init(int__Text3);
    Tflt_init(int__Text4);
  }

  /* control step */
  ctl_step();
}

void adc_dma_on_full(void) {
  VAL_T data[ADC_CHANNELS];
  uint8_t sample;
  uint8_t channel;
  
  for (channel = 0; channel < ADC_CHANNELS; channel ++) {
    VAL_T buf[ADC_SAMPLES];
    
    for (sample = 0; sample < ADC_SAMPLES; sample++) {
      buf[sample] = gen_make(VAL_T, adc_dma_data[sample]._[channel]);
    }

    data[channel] = scale_val_const(VAL_T,
                                    med_step(VAL_T, buf, ADC_SAMPLES, ADC_SAMPLES / 3),
                                    ADC_MIN, ADC_MAX,
                                    0, (1<<12)-1);
  }

  adc_conv(data);
  memset(adc_dma_data, 0, sizeof(adc_dma_data));
}
