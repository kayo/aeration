#include <ctl/type.h>
#include <ctl/util.h>

#include "hw.h"
#include "fw.h"
#include "upar/params.h"

#define PWM_TIM_PER (1e6/50)

#define PWM_VAL(val) ((val) * PWM_TIM_TOP / PWM_TIM_PER)

#define toP(type, val)                               \
  gen_int(type,                                      \
          scale_val_const(type,                      \
                          clamp_val_const(type,      \
                                          val,       \
                                          0.0, 1.0), \
                          0.0, 1.0,                  \
                          PWM_VAL(A_CLOSE),          \
                          PWM_VAL(A_OPEN)))

static VAL_T s = gen_make(VAL_T, 0.01);
static VAL_T t = 0;

void driver_init(void) {
  //pwm_tim_DRV2_PWM_put(toP(VAL_T, gen_make(VAL_T, 1.0)));
}

void driver_step(void) {
  VAL_T t_ = gen_add(VAL_T, t, s);
  if (gen_ge(VAL_T, t_, gen_0(VAL_T)) &&
      gen_le(VAL_T, t_, gen_1(VAL_T))) {
    t = t_;
  } else {
    s = gen_neg(VAL_T, s);
    t = gen_add(VAL_T, t, s);
  }
  pwm_tim_DRV1_PWM_put(toP(VAL_T, t));
}
