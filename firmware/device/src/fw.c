#include "fw.h"
#include "upar/params.h"

#define SYNC_CONV

#ifdef DEBUG_MEM_USAGE
mcu_memory_usage_t mem_usage;
#endif

Tenv_t Tenv[CHANNELS];

#define Tenv_set(type, kind, tval1, tval2) ({         \
      bool valid1 = temp_valid(type, tval1);          \
      bool valid2 = temp_valid(type, tval2);          \
      (valid1 && valid2 ?                             \
       kind##_val(type, tval1, tval2) :               \
       valid1 ? tval1 : valid2 ? tval2 :              \
       gen_make(type, T_INVALID));                    \
    })

#define Tenv_step(type, state, tval1, tval2) {        \
    (state)->min = Tenv_set(type, min, tval1, tval2); \
    (state)->max = Tenv_set(type, max, tval1, tval2); \
  }

void ctl_step(void) {
#if CHANNELS > 0
  Tenv_step(VAL_T, &Tenv[0], int__Text1, int__Text2);
#endif
#if CHANNELS > 1
  Tenv_step(VAL_T, &Tenv[1], int__Text3, int__Text4);
#endif
  
  if (ctl__state == enabled_state) {
    switch (ctl__mode) {
    case auto_mode:
      for (uint8_t i = 0; i < CHANNELS; i++) {
        if (gen_ge(VAL_T, Tenv[i].max, ctl__Tenv__open)) {
          servo_set_target(VAL_T, &flap_state[i], driver_open);
        }
        if (gen_le(VAL_T, Tenv[i].min, ctl__Tenv__close)) {
          servo_set_target(VAL_T, &flap_state[i], driver_close);
        }
      }
      break;
    case manual_mode:
      break;
    }
  }
  
  driver_step();

#ifdef DEBUG_MEM_USAGE
  mcu_memory_usage(&mem_usage);
#endif
}

volatile uint8_t events;

void clk_on_tick(void) {
  //iwd_renew();
  
  //evt_set(events, sys_ev_rtc);
  main_resume();
}

int main(void) {
  irq_init();
  clk_init();
  pio_init();
  adc_init();

  pwm_tim_init();
  srt_init();
  
  upar_init();
  driver_init();

#ifdef SYNC_CONV
  smp_tim_init();
#endif
  
  mcu_modes_on_off(mcu_sleep_on_exit,
                   mcu_event_on_interrupt);

  //iwd_start();
  //iwd_renew();

  for (;;) {
    main_suspend();

    uint8_t selected = evt_get(events, any_ev);

    if (evt_has(selected, srt_ev_req)) {
      srt_on_req();
      evt_clr(events, srt_ev_req);
    }

#ifdef DEBUG_MEM_USAGE
    mcu_memory_usage(&mem_usage);
#endif
  }
  
  srt_done();
  pwm_tim_done();
#ifdef SYNC_CONV
  smp_tim_done();
#endif
  adc_done();
  pio_done();
  clk_done();
  irq_done();

  return 0;
}
