#include "hw.h"
#include "fw.h"
#include "upar/params.h"

static int tx_resize(ustr_t *str, ulen_t len) {
  if (len > SRT_TX_SIZE) {
    return -1;
  } else if (len == 0) {
    ustr_len(str) = 0;
  }
  
  return 0;
}

static void rx_resume(void) {
  /* set the length of receiver buffer */
  srt_rx_dma_set_len(SRT_RX_SIZE);
  
  /* start receiver */
  srt_rx_dma_on();
}

void srt_on_req(void) {
  /* get length of received data */
  uint16_t rx_len = SRT_RX_SIZE - srt_rx_dma_get_len();
  
  /* handle command */
  ustr_t rx_str;
  
  ustr_extern_init(&rx_str, srt_dma_data.rx);
  ustr_len(&rx_str) = rx_len;

#ifdef CONSOLE_CRC32
  { /* calculate and check crc32 of data */
    hw_crc32_reset();
    
    ulen_t len = ustr_len(&rx_str) - sizeof(uint32_t);
    uint32_t crc = hw_crc32_update(ustr_str(&rx_str), len);
    uint32_t crc_in;
    
    ustr_pos(&rx_str) = len;
    ustr_getvr(&rx_str, crc_in);
    ustr_pos(&rx_str) = 0;
    ustr_len(&rx_str) = len;
    
    if (crc_in != crc) {
      goto resume;
    }
  }
#endif /* CONSOLE_CRC32 */
  
  ustr_t tx_str;
  ustr_extern_init(&tx_str, srt_dma_data.tx);
  tx_str.res = tx_resize;

  int res;

  for (; (res = upar_proc(&rx_str, &tx_str)) == 0 &&
         ustr_pos(&rx_str) < ustr_len(&rx_str); );
  
  if (res != 0) {
    goto resume;
  }
  
#ifdef CONSOLE_CRC32
  { /* calculate and append crc32 to data */
    hw_crc32_reset();
    
    uint32_t crc = hw_crc32_update(ustr_str(&tx_str),
                                   ustr_len(&tx_str));
    
    ustr_putvr(&tx_str, crc);
  }
#endif /* CONSOLE_CRC32 */
  
  /* set tx length */
  srt_tx_dma_set_len(ustr_len(&tx_str));
  
#ifdef pio_SRT_RE
  /* disable RS485 receiver */
  pio_SRT_RE_off();
#endif

#ifdef pio_SRT_DE
  /* enable RS485 driver */
  pio_SRT_DE_on();
#endif
  
  /* start transmitter */
  srt_tx_dma_on();

 resume:
  rx_resume();
}

void srt_rx_on_idle(void) {
  /* get length of received data */
  uint16_t rx_len = SRT_RX_SIZE - srt_rx_dma_get_len();
  
  if (rx_len == 0) {
    return;
  }
  
  /* disable receiver */
  srt_rx_dma_off();

#ifdef CONSOLE_CRC32
  if (rx_len < 4) {
    rx_resume();
    return;
  }
#endif /* CONSOLE_CRC32 */

  evt_set(events, srt_ev_req);
  main_resume();
}

/* transmission complete handler */
void srt_tx_on_done(void) {
  /* disable transmission complete handler */
  srt_on_off(srt_no_touch, srt_tx_done);
  
  /* set the length of receiver buffer */
  srt_rx_dma_set_len(SRT_RX_SIZE);

  /* start receiver */
  srt_rx_dma_on();

#ifdef pio_SRT_DE
  /* disable RS485 driver */
  pio_SRT_DE_off();
#endif

#ifdef pio_SRT_RE
  /* enable RS485 receiver */
  pio_SRT_RE_on();
#endif
}

void srt_tx_dma_on_full(void) {
  /* stop transmitter */
  srt_tx_dma_off();
  
  /* enable transmission complete handler to determine when transmission actually ended */
  srt_on_off(srt_tx_done, srt_no_touch);
}
