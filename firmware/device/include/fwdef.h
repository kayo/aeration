#ifndef FWDEF_H
#define FWDEF_H

typedef enum control_state {
  disabled_state,
  enabled_state,
} control_state_t;

typedef enum control_mode {
  manual_mode,
  auto_mode,
} control_mode_t;

#endif /* FWDEF_H */
