#include "fw.h"
#include "upar/params.h"

#define SYNC_CONV

#ifdef DEBUG_MEM_USAGE
mcu_memory_usage_t mem_usage;
#endif

void ctl_step(void) {
  driver_step();

#ifdef DEBUG_MEM_USAGE
  mcu_memory_usage(&mem_usage);
#endif
}

volatile uint8_t events;

void clk_on_tick(void) {
  iwd_renew();
  
  //evt_set(events, sys_ev_rtc);
  main_resume();
}

int main(void) {
  irq_init();
  clk_init();
  pio_init();
  adc_init();

  pwm_tim_init();
  srt_init();
  
  upar_init();
  driver_init();

#ifdef SYNC_CONV
  smp_tim_init();
#endif
  
  mcu_modes_on_off(mcu_sleep_on_exit,
                   mcu_event_on_interrupt);

  iwd_start();
  iwd_renew();

  for (;;) {
    main_suspend();

    uint8_t selected = evt_get(events, any_ev);

    if (evt_has(selected, srt_ev_req)) {
      srt_on_req();
      evt_clr(events, srt_ev_req);
    }

#ifdef DEBUG_MEM_USAGE
    mcu_memory_usage(&mem_usage);
#endif
  }
  
  srt_done();
  pwm_tim_done();
#ifdef SYNC_CONV
  smp_tim_done();
#endif
  adc_done();
  pio_done();
  clk_done();
  irq_done();

  return 0;
}
