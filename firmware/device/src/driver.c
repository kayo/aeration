#include <ctl/type.h>
#include <ctl/util.h>

#include "hw.h"
#include "fw.h"
#include "upar/params.h"

#include "servo.h"

#define PWM_TIM_PER (1e6/50)

#define PWM_VAL(val) ((val) * PWM_TIM_TOP / PWM_TIM_PER)

#define toP(type, val)                               \
  gen_int(type,                                      \
          scale_val_const(type,                      \
                          clamp_val_const(type,      \
                                          val,       \
                                          0.0, 1.0), \
                          0.0, 1.0,                  \
                          PWM_VAL(A_CLOSE),          \
                          PWM_VAL(A_OPEN)))

flap_param_t flap_param;
flap_state_t flap_state[CHANNELS];

void driver_init(void) {
  for (uint8_t i = 0; i < CHANNELS; i++) {
    servo_init(VAL_T, &flap_state[i]);
  }
}

void driver_step(void) {
  for (uint8_t i = 0; i < CHANNELS; i++) {
    servo_step(VAL_T, &flap_param, &flap_state[i]);
  }
#if CHANNELS > 0
  pwm_tim_DRV1_PWM_put(toP(VAL_T, flap_state[0].current));
#endif
#if CHANNELS > 1
  pwm_tim_DRV2_PWM_put(toP(VAL_T, flap_state[1].current));
#endif
}
