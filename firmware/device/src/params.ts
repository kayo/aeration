import { uparServer, uparAttrs } from "param";

interface Attrs {
    language: string,
    version: string,
    hasName: boolean,
    hasInfo: boolean,
    hasUnit: boolean,
    hasItem: boolean,
    channels: number;
}

const {
    language,
    version,
    hasName,
    hasInfo,
    hasUnit,
    hasItem,
    channels,
}: Attrs = uparAttrs({
    language: '_',
    version: '0.0.0',
    hasName: true,
    hasInfo: true,
    hasUnit: true,
    hasItem: true,
    channels: 2,
});

function l10n(strings: string | { [lang: string]: string }): string {
    if (typeof strings == 'string') {
        return strings;
    } else {
        return strings[language];
    }
}

type option<T> = T | undefined;

function name(str: string): option<string> {
    return hasName ? str : undefined;
}

function info(strings: string | { [lang: string]: string }): option<string> {
    return hasInfo ? l10n(strings) : undefined;
}

function unit(strings: string | { [lang: string]: string }): option<string> {
    return hasUnit ? l10n(strings) : undefined;
}

function item(strings: string | { [lang: string]: string }): option<string> {
    return hasItem ? l10n(strings) : undefined;
}

function iterate<T>(len: number, fn: (i: number) => T): T[] {
    const r: T[] = [];
    for (let i = 0; i < len; i++) r.push(fn(i));
    return r;
}

const byte = {
    bits: 8,
};

const word = {
    bits: 32,
};

const sens = {
    getable: true,
};

const ctrl = {
    ...sens,
    setable: true,
};

const pref = {
    persist: true,
};

const val = {
    type: 'sfix',
    ...word,
    frac: 16,
};

const temp = {
    unit: unit("°C"),
    ...val,
};

const part = {
    min: 0.0, max: 1.0,
    ...val,
};

const volt = {
    unit: unit({
        _: "V",
        ru: "В",
    }),
    ...val,
};

const sec = {
    unit: unit({
        _: "S",
        ru: "с",
    }),
    ...val,
};

const mode = {
    type: 'uint',
    ...byte,
};

const virt = {
    bind: true,
};

const state = {
    ...mode,
    opt: [
        {
            name: name('off'),
            val: 'disabled_state',
            info: info({
                _: "Disabled",
                ru: "Выключено",
            }),
        },
        {
            name: name('on'),
            val: 'enabled_state',
            info: info({
                _: "Enabled",
                ru: "Включено",
            }),
        }
    ],
};

uparServer({
    includes: [
        "fw.h",
    ],

    storage: {
        backend: 'mcuhw',
        uidType: 'djb2',
        section: 'par',
    },

    params: [
        {
            name: name('env'),
            info: info({
                _: "Environment state",
                ru: "Состояние среды",
            }),
            item: item({
                _: "Environ",
                ru: "Среда",
            }),
            params: [
                ...iterate(channels, i => ({
                    name: name(`T${i + 1}`),
                    info: info({
                        _: `Environment temperature ${i + 1}`,
                        ru: `Температура среды ${i + 1}`,
                    }),
                    params: [
                        {
                            name: name('min'),
                            info: info({
                                _: "Minimum",
                                ru: "Минимальная",
                            }),
                            ...temp, ...sens,
                            bind: `Tenv[${i}].min`
                        },
                        {
                            name: name('max'),
                            info: info({
                                _: "Maximum",
                                ru: "Максимальная",
                            }),
                            ...temp, ...sens,
                            bind: `Tenv[${i}].max`
                        },
                    ],
                })),
            ],
        },

        {
            name: name('act'),
            info: info({
                _: "Control state",
                ru: "Состояние управления",
            }),
            item: item({
                _: "State",
                ru: "Состояние",
            }),
            params: [
                {
                    name: name('Vent'),
                    info: info({
                        _: 'Vent position',
                        ru: 'Положение форточки'
                    }),
                    params: [
                        {
                            name: name('target'),
                            info: info({
                                _: 'Target',
                                ru: 'Целевое',
                            }),
                            params: [
                                ...iterate(channels, i => ({
                                    name: name(`f${i + 1}`),
                                    info: info({
                                        _: `Flap #${i + 1}`,
                                        ru: `Заслонка №${i + 1}`,
                                    }),
                                    ...part, ...sens,
                                    bind: `flap_state[${i}].target`,
                                }))
                            ],
                        },
                        {
                            name: name('current'),
                            info: info({
                                _: 'Current',
                                ru: 'Текущее',
                            }),
                            params: [
                                ...iterate(channels, i => ({
                                    name: name(`f${i + 1}`),
                                    info: info({
                                        _: `Flap #${i + 1}`,
                                        ru: `Заслонка №${i + 1}`,
                                    }),
                                    ...part, ...sens,
                                    bind: `flap_state[${i}].current`,
                                }))
                            ],
                        }
                    ]
                }
            ]
        },

        {
            name: name('ctl'),
            info: info({
                _: "Main controls",
                ru: "Основные настройки",
            }),
            params: [
                {
                    name: name('state'),
                    info: info({
                        _: "Control state",
                        ru: "Состояние управления",
                    }),
                    ...state, ...ctrl, ...pref,
                    def: 'enabled_state',
                },
                {
                    name: name('mode'),
                    info: info({
                        _: "Control mode",
                        ru: "Режим управления",
                    }),
                    ...mode, ...ctrl, ...pref,
                    def: 'auto_mode',
                    opt: [
                        {
                            name: name('auto'),
                            val: 'auto_mode',
                            info: info({
                                _: "Automatic",
                                ru: "Автоматический",
                            }),
                        },
                        {
                            name: name('manual'),
                            val: 'manual_mode',
                            info: info({
                                _: "Manual",
                                ru: "Ручной",
                            }),
                        },
                    ],
                },
                {
                    name: name('Tenv'),
                    info: info({
                        _: "Environment temperature",
                        ru: "Температура среды",
                    }),
                    params: [
                        {
                            name: name('open'),
                            info: info({
                                _: "Openning",
                                ru: "Открытия",
                            }),
                            def: 'T_OPEN', min: 18, max: 32,
                            ...temp, ...ctrl, ...pref,
                        },
                        {
                            name: name('close'),
                            info: info({
                                _: "Closing",
                                ru: "Закрытия",
                            }),
                            def: 'T_CLOSE', min: 18, max: 32,
                            ...temp, ...ctrl, ...pref,
                        },
                    ],
                }
            ],
        },

        {
            name: name('int'),
            info: info({
                _: "Internal state",
                ru: "Внутреннее состояние",
            }),
            params: [
                {
                    name: name('Uref'),
                    info: info({
                        _: "Reference voltage",
                        ru: "Опорное напряжение",
                    }),
                    ...volt, ...sens,
                },
                {
                    name: name('Tmcu'),
                    info: info({
                        _: "Microcontroller temperature",
                        ru: "Температура микроконтроллера",
                    }),
                    ...temp, ...sens,
                },
                ...iterate(channels * 2, i => ({
                    name: name(`Text${i + 1}`),
                    info: info({
                        _: `External temperature ${i + 1}`,
                        ru: `Внешняя температура ${i + 1}`,
                    }),
                    ...temp, ...sens,
                })),
            ],
        },

        {
            name: name('tun'),
            info: info({
                _: "Fine tuning",
                ru: "Тонкая настройка",
            }),
            params: [
                {
                    name: name('sens'),
                    info: info({
                        _: "Sensors",
                        ru: "Датчики",
                    }),
                    params: [
                        {
                            name: name('Tenv_int'),
                            info: info({
                                _: "Filtering interval for environment temperature",
                                ru: "Интервал фильтрации для температуры среды",
                            }),
                            ...sec, ...ctrl, ...pref,
                            def: 20, min: 0, max: 600,
                            postSet: "ewma_set_T(VAL_T, &Tenv_flt_param, <var>, CTL_PERIOD_VAL);",
                        }
                    ],
                },
                {
                    name: name('drv'),
                    info: info({
                        _: "Drivers",
                        ru: "Водители",
                    }),
                    params: [
                        {
                            name: name('Dwin'),
                            info: info({
                                _: "Openning/closing duration",
                                ru: "Длительность открытия/закрытия",
                            }),
                            ...sec, ...ctrl, ...pref,
                            def: 5, min: 1, max: 10,
                            postSet: "servo_set_time(VAL_T, &flap_param, <var>, CTL_PERIOD_VAL);",
                        }
                    ],
                }
            ],
        },

        {
            name: name('info'),
            info: info({
                _: "Device info",
                ru: "Информация об устройстве",
            }),
            params: [
                {
                    name: name('device'),
                    info: info({
                        _: "Device name",
                        ru: "Название устройства",
                    }),
                    type: 'cstr',
                    def: l10n({
                        _: "The climate control unit",
                        ru: "Устройство управления климатом",
                    }),
                    ...virt,
                },
                {
                    name: name('version'),
                    info: info({
                        _: "Version number",
                        ru: "Номер версии",
                    }),
                    type: 'cstr',
                    def: version,
                    ...virt,
                },
            ],
        },
    ],
});
