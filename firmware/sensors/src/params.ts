//import { UParDecl, UParNumCons, UParText } from "types";
import { uparServer, uparAttrs } from "param";

interface Attrs {
    language: string,
    version: string,
    hasName: boolean,
    hasInfo: boolean,
    hasUnit: boolean,
    hasItem: boolean,
}

const {
    language,
    version,
    hasName,
    hasInfo,
    hasUnit,
    hasItem,
}: Attrs = uparAttrs({
    language: '_',
    version: '0.0.0',
    hasName: true,
    hasInfo: true,
    hasUnit: true,
    hasItem: true,
});

function l10n(strings: string | { [lang: string]: string }): string {
    if (typeof strings == 'string') {
        return strings;
    } else {
        return strings[language];
    }
}

type option<T> = T | undefined;

function name(str: string): option<string> {
    return hasName ? str : undefined;
}

function info(strings: string | { [lang: string]: string }): option<string> {
    return hasInfo ? l10n(strings) : undefined;
}

function unit(strings: string | { [lang: string]: string }): option<string> {
    return hasUnit ? l10n(strings) : undefined;
}

function item(strings: string | { [lang: string]: string }): option<string> {
    return hasItem ? l10n(strings) : undefined;
}

const byte = {
    bits: 8,
};

const word = {
    bits: 32,
};

const sens = {
    getable: true,
};

const ctrl = {
    ...sens,
    setable: true,
};

const pref = {
    persist: true,
};

const val = {
    type: 'sfix',
    ...word,
    frac: 16,
};

const temp = {
    unit: unit("°C"),
    ...val,
};

const volt = {
    unit: unit({
        _: "V",
        ru: "В",
    }),
    ...val,
};

const sec = {
    unit: unit({
        _: "S",
        ru: "с",
    }),
    ...val,
};

const mode = {
    type: 'uint',
    ...byte,
};

const virt = {
    bind: true,
};

const state = {
    ...mode,
    opt: [
        {
            name: name('off'),
            val: 'disabled_state',
            info: info({
                _: "Disabled",
                ru: "Выключено",
            }),
        },
        {
            name: name('on'),
            val: 'enabled_state',
            info: info({
                _: "Enabled",
                ru: "Включено",
            }),
        }
    ],
};

const door_state = {
    ...mode,
    opt: [
        {
            name: name('closed'),
            val: 'disabled_state',
            info: info({
                _: "Closed",
                ru: "Закрыто",
            }),
        },
        {
            name: name('openned'),
            val: 'enabled_state',
            info: info({
                _: "Openned",
                ru: "Открыто",
            }),
        }
    ],
};

uparServer({
    includes: [
        "fw.h",
    ],

    storage: {
        backend: 'mcuhw',
        uidType: 'djb2',
        section: 'par',
    },

    params: [
        {
            name: name('env'),
            info: info({
                _: "Environment state",
                ru: "Состояние среды",
            }),
            item: item({
                _: "Environ",
                ru: "Среда",
            }),
            params: [
                {
                    name: name('Tair1'),
                    info: info({
                        _: "Environment temperature 1",
                        ru: "Температура среды 1",
                    }),
                    params: [
                        {
                            name: name('min'),
                            info: info({
                                _: "Minimum",
                                ru: "Минимальная",
                            }),
                            ...temp, ...sens,
                        },
                        {
                            name: name('max'),
                            info: info({
                                _: "Maximum",
                                ru: "Максимальная",
                            }),
                            ...temp, ...sens,
                        },
                    ],
                },
                {
                    name: name('Tair2'),
                    info: info({
                        _: "Environment temperature 2",
                        ru: "Температура среды 2",
                    }),
                    params: [
                        {
                            name: name('min'),
                            info: info({
                                _: "Minimum",
                                ru: "Минимальная",
                            }),
                            ...temp, ...sens,
                        },
                        {
                            name: name('max'),
                            info: info({
                                _: "Maximum",
                                ru: "Максимальная",
                            }),
                            ...temp, ...sens,
                        },
                    ],
                },
            ],
        },

        {
            name: name('act'),
            info: info({
                _: "Control state",
                ru: "Состояние управления",
            }),
            item: item({
                _: "State",
                ru: "Состояние",
            }),
            params: [
                {
                    name: name('Sdoor1'),
                    info: info({
                        _: "Door position 1",
                        ru: "Положение двери 1",
                    }),
                    ...door_state, ...sens,
                },
                {
                    name: name('Sdoor2'),
                    info: info({
                        _: "Door position 2",
                        ru: "Положение двери 2",
                    }),
                    ...door_state, ...sens,
                }
            ]
        },

        {
            name: name('ctl'),
            info: info({
                _: "Main controls",
                ru: "Основные настройки",
            }),
            params: [
                {
                    name: name('state'),
                    info: info({
                        _: "Control state",
                        ru: "Состояние управления",
                    }),
                    ...state, ...ctrl, ...pref,
                    def: 'enabled_state',
                },
                {
                    name: name('mode'),
                    info: info({
                        _: "Control mode",
                        ru: "Режим управления",
                    }),
                    ...mode, ...ctrl, ...pref,
                    def: 'auto_mode',
                    opt: [
                        {
                            name: name('auto'),
                            val: 'auto_mode',
                            info: info({
                                _: "Automatic",
                                ru: "Автоматический",
                            }),
                        },
                        {
                            name: name('manual'),
                            val: 'manual_mode',
                            info: info({
                                _: "Manual",
                                ru: "Ручной",
                            }),
                        },
                    ],
                },
                {
                    name: name('Tair'),
                    info: info({
                        _: "Environment temperature",
                        ru: "Температура среды",
                    }),
                    params: [
                        {
                            name: name('open'),
                            info: info({
                                _: "Openning",
                                ru: "Открытия",
                            }),
                            def: 22, min: 18, max: 32,
                            ...temp, ...ctrl, ...pref,
                        },
                        {
                            name: name('close'),
                            info: info({
                                _: "Closing",
                                ru: "Закрытия",
                            }),
                            def: 20, min: 18, max: 32,
                            ...temp, ...ctrl, ...pref,
                        },
                    ],
                }
            ],
        },

        {
            name: name('int'),
            info: info({
                _: "Internal state",
                ru: "Внутреннее состояние",
            }),
            params: [
                {
                    name: name('Uref'),
                    info: info({
                        _: "Reference voltage",
                        ru: "Опорное напряжение",
                    }),
                    ...volt, ...sens,
                },
                {
                    name: name('Tmcu'),
                    info: info({
                        _: "Microcontroller temperature",
                        ru: "Температура микроконтроллера",
                    }),
                    ...temp, ...sens,
                },
                ...((n) => {
                    const out = [];
                    for (let i = 1; i <= n; i++) {
                        out.push({
                            name: name(`Text${i}`),
                            info: info({
                                _: `External temperature ${i}`,
                                ru: `Внешняя температура ${i}`,
                            }),
                            ...temp, ...sens,
                        });
                    }
                    return out;
                })(4),
            ],
        },

        {
            name: name('tun'),
            info: info({
                _: "Fine tuning",
                ru: "Тонкая настройка",
            }),
            params: [
                {
                    name: name('sens'),
                    info: info({
                        _: "Sensors",
                        ru: "Датчики",
                    }),
                    params: [
                        {
                            name: name('Tair_int'),
                            info: info({
                                _: "Filtering interval for environment temperature",
                                ru: "Интервал фильтрации для температуры среды",
                            }),
                            ...sec, ...ctrl, ...pref,
                            def: 20, min: 0, max: 600,
                            postSet: "ewma_set_T(VAL_T, &Tair_flt_param, <var>, CTL_PERIOD_VAL);",
                        }
                    ],
                },
            ],
        },

        {
            name: name('info'),
            info: info({
                _: "Device info",
                ru: "Информация об устройстве",
            }),
            params: [
                {
                    name: name('device'),
                    info: info({
                        _: "Device name",
                        ru: "Название устройства",
                    }),
                    type: 'cstr',
                    def: l10n({
                        _: "The climate control unit",
                        ru: "Устройство управления климатом",
                    }),
                    ...virt,
                },
                {
                    name: name('version'),
                    info: info({
                        _: "Version number",
                        ru: "Номер версии",
                    }),
                    type: 'cstr',
                    def: version,
                    ...virt,
                },
            ],
        },
    ],
});
