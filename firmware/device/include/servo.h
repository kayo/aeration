#ifndef __SERVO_H__
#define __SERVO_H__

#include <ctl/type.h>
#include <ctl/util.h>

#define servo_param_t(type) \
  struct {                  \
    type step;              \
  }

#define servo_state_t(type) \
  struct {                  \
    /* target position */   \
    type target;            \
    /* current position */  \
    type current;           \
  }

#define servo_set_time(type, param, time, period) \
  (param)->step = gen_div(type, period, time)

#define servo_make(type) { gen_0(type), gen_0(type) }

#define servo_set_target(type, state, value)    \
  ((state)->target = (value))

#define servo_init(type, state) {               \
    (state)->target = gen_0(type);              \
    (state)->current = gen_0(type);             \
  }

#define servo_next(type, target, current, step) \
  gen_lt(type, current, target) ?               \
  min_val(type, target,                         \
          gen_add(type, current, step)) :       \
  gen_gt(type, current, target) ?               \
  max_val(type, target,                         \
          gen_sub(type, current, step)) :       \
  (current)

#define servo_step(type, param, state)          \
  (state)->current =                            \
    servo_next(type, (state)->target,           \
               (state)->current,                \
               (param)->step)

#endif /* __SERVO_H__ */
